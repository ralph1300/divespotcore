-- this is a backup of the latest database structure, it can be used to rebuild the database.

-- phpMyAdmin SQL Dump
-- version 4.6.6deb4+deb9u2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 04, 2021 at 07:18 PM
-- Server version: 10.1.48-MariaDB-0+deb9u2
-- PHP Version: 7.0.33-0+deb9u10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `divespotaustria`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminuser`
--

CREATE TABLE `adminuser` (
  `user` varchar(200) COLLATE utf8_bin NOT NULL,
  `password` varchar(200) COLLATE utf8_bin NOT NULL,
  `userID` int(11) NOT NULL,
  `userSession` varchar(200) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `adminuser`
--

INSERT INTO `adminuser` (`user`, `password`, `userID`, `userSession`) VALUES
('test', 'test_pass', 1, 'usar1n9ero5er1gjjnecdnggt5');

-- --------------------------------------------------------

--
-- Table structure for table `DivingPlace`
--

CREATE TABLE `DivingPlace` (
  `name` varchar(200) COLLATE utf8_bin NOT NULL,
  `text` varchar(500) COLLATE utf8_bin NOT NULL,
  `alternateText` varchar(500) COLLATE utf8_bin NOT NULL DEFAULT '""',
  `latitude` varchar(50) COLLATE utf8_bin NOT NULL,
  `longitude` varchar(50) COLLATE utf8_bin NOT NULL,
  `lakeID` int(11) NOT NULL,
  `placeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `divingSchool`
--

CREATE TABLE `divingSchool` (
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `text` varchar(500) COLLATE utf8_bin NOT NULL,
  `alternateText` varchar(500) COLLATE utf8_bin NOT NULL,
  `webURL` varchar(100) COLLATE utf8_bin NOT NULL,
  `latitude` varchar(50) COLLATE utf8_bin NOT NULL,
  `longitude` varchar(50) COLLATE utf8_bin NOT NULL,
  `lakeID` int(11) NOT NULL,
  `schooldID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `lake`
--

CREATE TABLE `lake` (
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `latitude` varchar(50) COLLATE utf8_bin NOT NULL,
  `longitude` varchar(50) COLLATE utf8_bin NOT NULL,
  `lockedForPremium` tinyint(1) NOT NULL,
  `isShop` tinyint(1) NOT NULL,
  `lakeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `placeImage`
--

CREATE TABLE `placeImage` (
  `placeID` int(11) NOT NULL,
  `url` varchar(200) COLLATE utf8_bin NOT NULL,
  `orginName` varchar(200) COLLATE utf8_bin NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `isMainImg` tinyint(1) NOT NULL,
  `placeImageID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `depth` varchar(50) COLLATE utf8_bin NOT NULL,
  `divingPlaceID` int(11) NOT NULL,
  `quality` int(11) NOT NULL,
  `difficulty` int(11) NOT NULL,
  `fish` int(11) NOT NULL,
  `plants` int(11) NOT NULL,
  `nightdive` int(11) NOT NULL,
  `ratingID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `schoolImage`
--

CREATE TABLE `schoolImage` (
  `schoolID` int(11) NOT NULL,
  `url` varchar(200) COLLATE utf8_bin NOT NULL,
  `orginName` varchar(200) COLLATE utf8_bin NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `isMainImg` tinyint(1) NOT NULL,
  `schoolImageID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminuser`
--
ALTER TABLE `adminuser`
  ADD PRIMARY KEY (`userID`);

--
-- Indexes for table `DivingPlace`
--
ALTER TABLE `DivingPlace`
  ADD PRIMARY KEY (`placeID`);

--
-- Indexes for table `divingSchool`
--
ALTER TABLE `divingSchool`
  ADD PRIMARY KEY (`schooldID`);

--
-- Indexes for table `lake`
--
ALTER TABLE `lake`
  ADD PRIMARY KEY (`lakeID`);

--
-- Indexes for table `placeImage`
--
ALTER TABLE `placeImage`
  ADD PRIMARY KEY (`placeImageID`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`ratingID`);

--
-- Indexes for table `schoolImage`
--
ALTER TABLE `schoolImage`
  ADD PRIMARY KEY (`schoolImageID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminuser`
--
ALTER TABLE `adminuser`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `DivingPlace`
--
ALTER TABLE `DivingPlace`
  MODIFY `placeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `divingSchool`
--
ALTER TABLE `divingSchool`
  MODIFY `schooldID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lake`
--
ALTER TABLE `lake`
  MODIFY `lakeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `placeImage`
--
ALTER TABLE `placeImage`
  MODIFY `placeImageID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `ratingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `schoolImage`
--
ALTER TABLE `schoolImage`
  MODIFY `schoolImageID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
