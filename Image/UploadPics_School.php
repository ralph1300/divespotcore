<!DOCTYPE html >
<html>
<?php
include("../initiateDB.php");
?>
<head>
    <link href="../Styles/BackgroundStyle.css" rel="stylesheet" type="text/css">
    <link href="../Styles/NavigationStyle.css" rel="stylesheet" type="text/css">
    <!--<link href= "Styles/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css">-->
    <script src="../JQuery/jquery-1.7.1.js"></script>
    <script src="../JQuery/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="../JQuery/jquery.ui.widget.js"></script>
    <script src="../JQuery/jquery.ui.tabs.js"></script>
    <script src="../JQuery/jquery.ui.button.js"></script>
    <script src="../JQuery/jquery.ui.accordion.js"></script>
    <script src="../JQuery/jquery.ui.core.js"></script>
    <script src="../JQuery/jquery.ui.datepicker.js"></script>
    <script src="../JS/VScript.js"></script>
    <title>Bilderupload</title>
</head>
<body>
<div id="Navigation">
    <table class="TopTable" id="HeadTable">
        <tr>
            <td>
                <a href="../EditStartpage.html" title="zur Startseite">
                    <img id="LogoNavi" alt="Applicationlogo" src="../Data/DSA_logo.png" width="100" height="100">
                </a>
            </td>
            <td>
                <h1>Bilder hochgeladen!</h1>
                <?php

                $placeName = html_entity_decode(utf8_decode(urldecode($_POST['SchoolPICS'])));
                $getPlace = "SELECT * FROM divingSchool WHERE name= '$placeName'";
                $resultPlace = mysqli_query($connection, $getPlace);
                if ($resultPlace) {
                    $Place = mysqli_fetch_assoc($resultPlace);
                }
                $placeID = $Place['placeID'];
                // Erlaubte Dateiendungen
                $allowedExtensions = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                // Maximale Größe der Datei
                //$maxSize = 2097152;
                // Hilfsvariable für Array Index
                $i = 0;
                foreach ($_FILES as $file) :
                    foreach ($file['name'] as $filename) :
                        $extension = getExtension($filename);
                        if (!in_array($extension, $allowedExtensions)) {
                            echo 'FORBIDDEN EXTENSION', $filename;
                            ++$i;
                            // Datei überspringen falls Endung nicht erlaubt
                            continue;
                        }
                        //Kontrolle Filegröße
                        //if($file['size'][$i] > $maxSize) {
                        //echo 'FILE TO BIG';
                        //++$i;
                        //continue;
                        //}
                        // Eindeutiger Dateiname
                        $uniqueIdentifier = md5(uniqid(rand(), true));
                        $extension = getExtension($filename);
                        $uniqueIdentifier = $uniqueIdentifier . "." .  $extension;
                        $save_as_name = $filename;
                        // Datei auf Server/in DB speichern und hoehe,breite usw dafür auslesen
                        $imgsize = getimagesize($file['tmp_name'][$i]);
                        $width = $imgsize[0];
                        $height = $imgsize[1];
                        move_uploaded_file($file['tmp_name'][$i], '../../uploads/'. $uniqueIdentifier);
                        saveThumbnailOfImage('../../uploads/'. $uniqueIdentifier ,$uniqueIdentifier);
                        //echo $filename;
                        $savePic = "INSERT INTO schoolImage(schoolID,url,orginName,width,height,isMainImg) VALUES(" . $placeID . ",'" . $uniqueIdentifier . "','" . $filename . "'," . $width . "," . $height . "," . 0 . ")";
                        mysqli_query($connection,$savePic);
                        ++$i;
                    endforeach;
                endforeach;
                ?>
            </td>
        </tr>
    </table>
</div>
<?php
function getExtension($name)
{
    return (false === ($p = strrpos($name, '.')) ? '' : substr($name, ++$p));
}
function saveThumbnailOfImage($path,$fileName)
{
    $img = imagecreatefromjpeg($path);
    $thumbWidth = 100;
    $width = imagesx( $img );
    $height = imagesy( $img );

    // calculate thumbnail size
    $new_width = $thumbWidth;
    $new_height = floor( $height * ( $thumbWidth / $width ) );
    
    // create a new temporary image
    $tmp_img = imagecreatetruecolor( $new_width, $new_height );

    // copy and resize old image into new image
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

    // save thumbnail into a file
    imagejpeg( $tmp_img,'../../uploads/thumb/' . $fileName );
}
?>
<p>Ihre Bilder wurden erfolgreich hinzugefuegt. Zum Verlassen der Seite bitte auf das Logo oder den Button drücken.</p>
<input type="button" value="Zurück" onclick="goBack()">
</body>
</html>
