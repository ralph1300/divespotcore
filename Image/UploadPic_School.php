<!DOCTYPE html >
<html>
<?php
include("../initiateDB.php");
?>
<head>
    <link href="../Styles/BackgroundStyle.css" rel="stylesheet" type="text/css">
    <link href="../Styles/NavigationStyle.css" rel="stylesheet" type="text/css">
    <!--<link href= "Styles/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css">-->
    <script src="../JQuery/jquery-1.7.1.js"></script>
    <script src="../JQuery/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="../JQuery/jquery.ui.widget.js"></script>
    <script src="../JQuery/jquery.ui.tabs.js"></script>
    <script src="../JQuery/jquery.ui.button.js"></script>
    <script src="../JQuery/jquery.ui.accordion.js"></script>
    <script src="../JQuery/jquery.ui.core.js"></script>
    <script src="../JQuery/jquery.ui.datepicker.js"></script>
    <script src="../JS/VScript.js"></script>
</head>
<body>
<div id="Navigation">
    <table class="TopTable" id="HeadTable">
        <tr>
            <td>
                <a href="../EditStartpage.html" title="zur Startseite">
                    <img id="LogoNavi" alt="Applicationlogo" src="../Data/DSA_logo.png" width="100" height="100">
                </a>
            </td>

            <td>
                <h1>Bild hochgeladen!</h1>
                <?php

                $dateityp = getimagesize($_FILES['userfile']['tmp_name']);
                if ($dateityp[2] != 0) {
                    $name = html_entity_decode(utf8_decode(urldecode($_POST['SchoolPIC'])));
                    $width = $dateityp[0];
                    $height = $dateityp[1];

                    //get place
                    $getPlace = "SELECT * FROM divingSchool WHERE name= '$name'";
                    $resultPlace = mysqli_query($connection, $getPlace);
                    if ($resultPlace) {
                        $place = mysqli_fetch_assoc($resultPlace);
                    }
                    $placeID = $place['placeID'];



                    //DELETE OLD MAINIMG IF NEEDED
                    $deleteOldMainImg = "DELETE FROM schoolImage WHERE schoolID = '$placeID' AND isMainImg=1";
                    mysqli_query($connection, $deleteOldMainImg);


                    $uniqueIdentifier = md5(uniqid(rand(), true));

                    $filename = $_FILES['userfile']['name'];
                    $extension = getExtension($filename);
                    $uniqueIdentifier = $uniqueIdentifier . "." .  $extension;

                    if (move_uploaded_file($_FILES['userfile']['tmp_name'], '../../uploads/'. $uniqueIdentifier)) {
                        echo "<p>Die Datei mit dem Namen " . $_FILES['userfile']['name'] . " wurde erfolgreich hochgeladen und zur Tauchbasis " . $name . " hinzugefuegt<p>";

                        $altText = "tauchplatz";
                        $savePic = "INSERT INTO schoolImage(schoolID,url,orginName,width,height,isMainImg) VALUES(" . $placeID . ",'" . $uniqueIdentifier . "','" . $filename .  "'," . $width . "," . $height . "," . 1 . ")";
                        mysqli_query($connection, $savePic);
                    } else {
                        echo "somethings not right";
                    }
                }
                function getExtension($name)
                {
                    return (false === ($p = strrpos($name, '.')) ? '' : substr($name, ++$p));
                }

                ?>
            </td>
        </tr>
    </table>
</div>

<p>Ihr Bild wurde erfolgreich hinzugefuegt. Zum Verlassen der Seite bitte auf das Logo oder den Button druecken.</p>
<input type="button" value="Zurueck" onclick="goBack()">
</body>
</html>
