<!DOCTYPE html >
<html>
<?php
include("../initiateDB.php");
?>
<head>
    <link href="Styles/BackgroundStyle.css" rel="stylesheet" type="text/css">
    <link href="Styles/NavigationStyle.css" rel="stylesheet" type="text/css">
    <!--<link href= "Styles/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css">-->
    <script src="JQuery/jquery-1.7.1.js"></script>
    <script src="JQuery/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="JQuery/jquery.ui.widget.js"></script>
    <script src="JQuery/jquery.ui.tabs.js"></script>
    <script src="JQuery/jquery.ui.button.js"></script>
    <script src="JQuery/jquery.ui.accordion.js"></script>
    <script src="JQuery/jquery.ui.core.js"></script>
    <script src="JQuery/jquery.ui.datepicker.js"></script>
    <script src="JS/VScript.js"></script>
    <script>
        $(function () {
            $("#Bildaccord").accordion();
        });
    </script>
    <title>Bildupload</title>
    <style type="text/css">
        .auto-style1 {
            margin-left: 40px;
        }
    </style>
</head>

<body>
<h2>Tauchplatz</h2>
<div id="Bildaccord">


    <h3>Tauchplatz</h3>
    <div>
        <input type="radio" name="Typ" value="Titelbild" onchange="changeProfil()" checked="checked"> Titelbild
        <input type="radio" name="Typ" value="Mehrere Bilder" onchange="changePics()"> Mehrere Bilder
        <div id="PROFILE">
            <p>Titelbildupload</p>
            <p>Falls ein neues Titelbild gewünscht ist, einfach ein neues hochladen, das alte wird dann gelöscht.</p>
            <form action="Image/UploadPic_Place.php" enctype="multipart/form-data" method="POST">
                <p>Tauchplatz</p><select name="placePIC">
                    <?php
                    $statement = 'SELECT * FROM DivingPlace ORDER BY name';
                    $result = mysqli_query($connection, $statement);
                    while ($ver = mysqli_fetch_assoc($result)) {
                        echo '<option>' . utf8_encode($ver['name']) . '</option>';
                    }
                    ?>
                    <br>
                    <input name="userfile" type="file">
                    <br>
                    <input type="submit" value="Hochladen">
            </form>
        </div>
        <div id="PICS">
            <p>Mehrfachupload</p>
            <form action="Image/UploadPics_Place.php" method="post" enctype="multipart/form-data">
                <p>Tauchplatz</p><select name="placePICS" id="PlacePICS">
                    <?php
                    $statement = 'SELECT * FROM DivingPlace ORDER BY name';
                    $result = mysqli_query($connection, $statement);
                    while ($ver = mysqli_fetch_assoc($result)) {
                        echo '<option>' . utf8_encode($ver['name']) . '</option>';
                    }
                    ?>
                </select>
                <input name="uploads[]" type="file" multiple>
                <br>
                <input type="submit" value="Hochladen">
                <p>Alle Bilder zu diesem Tauchplatz löschen(Warnung:Nicht umkehrbar!)</p><input type="button" value="Löschen" onclick="deleteImagesFromPlace()">
            </form>
        </div>

    </div>
    <h3>Tauchbasis</h3>
    <div>
        <input type="radio" name="Typ" value="Titelbild" onchange="changeProfilInfo()"> Titelbild
        <input type="radio" name="Typ" value="Mehrere Bilder" onchange="changePicsInfo()"> Mehrere Bilder <br>
        <div id="PROFILEINFO">
            <p>Titelbildupload</p>
            <p>Falls ein neues Titelbild gewünscht ist, einfach ein neues hochladen, das alte wird dann gelöscht.</p>
            <form action="Image/UploadPic_School.php" enctype="multipart/form-data" METHOD="POST">
                <p>Tauchbasis</p><select name="SchoolPIC" id="SchoolPIC">
                    <?php
                    $statement = 'SELECT * FROM divingSchool ORDER BY name';
                    $result = mysqli_query($connection, $statement);
                    while ($ver = mysqli_fetch_assoc($result)) {
                        echo '<option>' . utf8_encode($ver['name']) . '</option>';
                    }
                    ?>
                </select>
                
                <input name="userfile" type="file">
                <br>
                <input type="submit" value="Hochladen">
            </form>
        </div>
        <div id="PICSINFO">
            <p>Mehrfachupload</p>
            <form action="Image/UploadPics_School.php" method="post" enctype="multipart/form-data">
                <p>Tauchbasis</p><select name="SchoolPICS">
                    <?php

                    $statement = 'SELECT * FROM divingSchool ORDER BY name';
                    $result = mysqli_query($connection, $statement);
                    while ($ver = mysqli_fetch_assoc($result)) {
                        echo '<option>' . utf8_encode($ver['name']) . '</option>';
                    }


                    ?>
                </select>
                <input name="uploads[]" type="file" multiple>
                <br>
                <input type="submit" value="Hochladen">
                <p>Alle Bilder zu dieser Tauchbasis löschen(Warnung:Nicht umkehrbar!)</p><input type="button" value="Löschen" onclick="deleteImagesFromSchool()">
            </form>
        </div>


    </div>
</div>
</body>

</html>
