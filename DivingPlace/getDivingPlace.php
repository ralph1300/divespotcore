<?php
include("../initiateDB.php");
?>
<?php
$name = html_entity_decode(utf8_decode(urldecode($_REQUEST['name'])));

$getPlace = "SELECT * FROM DivingPlace WHERE name= '$name'";
$resultPlace = mysqli_query($connection, $getPlace);
if ($resultPlace) {
    $place = mysqli_fetch_assoc($resultPlace);
}
$placeID = $place['placeID'];
//get lake
$lakeID = $place['lakeID'];
$getLake = "SELECT * FROM lake WHERE lakeID='$lakeID'";
$resultLake = mysqli_query($connection, $getLake);
if ($resultLake) {
    $lake = mysqli_fetch_assoc($resultLake);
}

//get rating
$getRating = "SELECT * FROM rating WHERE divingPlaceID='$placeID'";
$resultRating = mysqli_query($connection, $getRating);
if ($resultRating) {
    $rating = mysqli_fetch_assoc($resultRating);
}


//set returnvalues
$lakeName = $lake['name'];
$name = $place['name'];
$text = $place['text'];
$textALT = $place['alternateText'];
$lati = $place['latitude'];
$long = $place['longitude'];
$qual = $rating['quality'];
$diff = $rating['difficulty'];
$fish = $rating['fish'];
$plant = $rating['plants'];
$night = $rating['nightdive'];
$depth = $rating['depth'];

echo json_encode(utf8_encode($lakeName . "|" . $name . "|" . $text . "|" . $textALT . "|" . $lati . "|" . $long. "|" . $qual. "|" . $diff. "|" . $fish. "|" . $plant. "|" . $night. "|" . $depth));


?>