<?php
/**
 * Created by PhpStorm.
 * User: ralphschnalzenberger
 * Date: 16/06/16
 * Time: 15:44
 */
include("../initiateDB.php");

//mysqli_set_charset($connection, 'utf8');

$lakeName = html_entity_decode(utf8_decode(urldecode($_REQUEST['lakeName'])));

$getLake = "SELECT * FROM lake WHERE name='$lakeName'";
$resultLake = mysqli_query($connection, $getLake);
if ($resultLake) {
    $lake = mysqli_fetch_assoc($resultLake);
}

$id = $lake['lakeID'];


//$getPlaces = "SELECT DivingPlace.placeID,name,text,alternateText,latitude,longitude,lakeID, placeImage.url
//              FROM DivingPlace
//              LEFT JOIN placeImage ON (DivingPlace.placeID=placeImage.placeID AND placeImage.isMainImg=1)
//              WHERE lakeID='$id'";

$getPlaces = "SELECT DivingPlace.placeID,name,text,alternateText,latitude,longitude,lakeID, placeImage.url
              FROM DivingPlace
              LEFT JOIN placeImage ON (DivingPlace.placeID=placeImage.placeID AND placeImage.isMainImg=1)
              WHERE lakeID='$id'";

$resultPlaces = mysqli_query($connection, $getPlaces);

$returnstring = "";
while ($place = mysqli_fetch_assoc($resultPlaces)) {
    $placeID = $place['placeID'];
    $getImageCount = "SELECT COUNT(url) as total FROM placeImage WHERE placeID ='$placeID' AND isMainImg=0";
    $imageCountResult = mysqli_query($connection, $getImageCount);
    if($imageCountResult){
        $count = mysqli_fetch_assoc($imageCountResult);
    }
    $getProperImageNames = "SELECT * FROM placeImage WHERE placeID ='$placeID' AND isMainImg=0";
    $nameResult = mysqli_query($connection, $getProperImageNames);
    $names = "";
    while($img = mysqli_fetch_assoc($nameResult)) {
        $names = $names . "-" .$img['url'];
    }
    $returnstring = $returnstring . '|' . $place['name'] . ";" . $place['url'] . ";" . $names . ";" . $count['total'];
}
echo json_encode(utf8_encode($returnstring));



