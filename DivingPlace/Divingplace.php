<!DOCTYPE html >
<?php
include("../initiateDB.php");
?>
<html>
<head>
    <link href="Styles/BackgroundStyle.css" rel="stylesheet" type="text/css">
    <link href="Styles/NavigationStyle.css" rel="stylesheet" type="text/css">
    <!--<link href= "Styles/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css">-->
    <script src="JQuery/jquery-1.7.1.js"></script>
    <script src="JQuery/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="JQuery/jquery.ui.widget.js"></script>
    <script src="JQuery/jquery.ui.tabs.js"></script>
    <script src="JQuery/jquery.ui.button.js"></script>
    <script src="JQuery/jquery.ui.accordion.js"></script>
    <script src="JQuery/jquery.ui.core.js"></script>
    <script src="JQuery/jquery.ui.datepicker.js"></script>
    <script src="JS/VScript.js"></script>
    <script>
        $(function () {
            $("#Veransaccord").accordion();
        });
    </script>

    <title>Tauchplatz</title>
</head>
<body>
<h2>Tauchplatz</h2>
<div id="Veransaccord">
    <h3>Übersicht</h3>
    <div>
        <p>See/Gebiet:</p><select id="filterLakeForSummary" onchange="loadSummaryForDivePlace()">
            <option></option>
            <?php
            $statement = 'SELECT * FROM lake ORDER BY name';
            $result = mysqli_query($connection, $statement);
            while ($St = mysqli_fetch_assoc($result)) {
                echo '<option>' . utf8_encode($St['name']) . '</option>';
            }
            ?>
        </select>

        <div style="height:130px; width:450px;">
            <table width="400" border="1" cellspacing="1" cellpadding="2" id="summaryTable">
            <tr>
                <td>
                    Name
                </td>
                <td>
                    Titelbild hochgeladen?
                </td>
                <td>
                    Hochgeladenen Bilder
                </td>
                <td>
                    Thumbnails
                </td>
            </tr>
            </table>
        </div>

    </div>
    <h3>Erstellen</h3>
    <div>
        <p>Name:</p><input type="text" id="dpNAME">
        <table>
            <tr>
                <td>
                    <p>Breitengrad:</p><input type="text" id="dpLONG"> Bsp.: 48.1234
                </td>
                <td>
                    <p>Längengrad:</p><input type="text" id="dpLAT"> Bsp.: 12.1234
                </td>
            </tr>
            <tr>
                <td>
                    <p>See/Gebiet:</p><select id="dpLAKE">
                        <?php
                        $statement = 'SELECT * FROM lake ORDER BY name';
                        $result = mysqli_query($connection, $statement);
                        while ($St = mysqli_fetch_assoc($result)) {
                            echo '<option>' . utf8_encode($St['name']) . '</option>';
                        }
                        ?>
                    </select>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <p>Beschreibung:</p><textarea id="dpTEXT" rows="5" cols="30"></textarea>
                </td>
                <td>
                    <p>Beschreibung Englisch:</p><textarea id="dpTEXTALT" rows="5" cols="30"></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Qualität:</p><input type="text" id="dpQual"> Ganze Zahl, 1-5
                </td>
                <td>
                    <p>Schwierigkeit:</p><input type="text" id="dpDiff"> Ganze Zahl, 1-5
                </td>
            </tr>
            <tr>
                <td>
                    <p>Fische:</p><input type="text" id="dpFISH"> Ganze Zahl, 1-3
                </td>
                <td>
                    <p>Pflanzen:</p><input type="text" id="dpPLANT">Ganze Zahl, 1-3
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nachttauchen:</p><input type="text" id="dpNIGHT">Ganze Zahl, 1-3
                </td>
                <td>
                    <p>Tiefe:</p><input type="text" id="dpDEPTH">
                </td>
            </tr>
        </table>
        <input type="button" value="Speichern" onclick="saveDivingPlace()">
    </div>


    <h3>Ändern</h3>
    <div>
        <table>
            <tr>
                <td>
                    <p>See/Gebiet-Filter:</p><select id="LAKECHANGEFORPLACE" onchange="updateDivingPlacesWithLake()">
                        <option></option>
                        <?php
                        $statement = 'SELECT * FROM lake ORDER BY name';
                        $result = mysqli_query($connection, $statement);
                        while ($St = mysqli_fetch_assoc($result)) {
                            echo '<option>' . utf8_encode($St['name']) . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tauchplatz</p><select id="DIVINGPLACECHANGE" onchange="updateCHDivingPlace()">
                        <option></option>
<!--                        --><?php
//
//                        $statement = 'SELECT * FROM DivingPlace ORDER BY name';
//                        $result = mysqli_query($connection, $statement);
//                        while ($ver = mysqli_fetch_assoc($result)) {
//                            echo '<option>' . utf8_encode($ver['name']) . '</option>';
//                        }
                        ?>
                    </select>
                </td>
                <td>
                    <p>Name:</p><input type="text" id="dpNAMEChange">
                </td>
            </tr>
            <tr>
                <td>
                    <p>Breitengrad:</p><input type="text" id="dpLONGChange"> Bsp.: 48.1234
                </td>
                <td>
                    <p>Längengrad:</p><input type="text" id="dpLATChange"> Bsp.: 12.1234
                </td>
            </tr>
            <tr>
                <td>
                    <p>See/Gebiet:</p><select id="dpLAKEChange">
                        <?php
                        $statement = 'SELECT * FROM lake ORDER BY name';
                        $result = mysqli_query($connection, $statement);
                        while ($St = mysqli_fetch_assoc($result)) {
                            echo '<option>' . utf8_encode($St['name']) . '</option>';
                        }
                        ?>
                    </select>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <p>Beschreibung:</p><textarea id="dpTEXTChange" rows="5" cols="30"></textarea>
                </td>
                <td>
                    <p>Beschreibung Englisch:</p><textarea id="dpTEXTALTChange" rows="5" cols="30"></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Qualität:</p><input type="text" id="dpQualChange"> Ganze Zahl, 1-5
                </td>
                <td>
                    <p>Schwierigkeit:</p><input type="text" id="dpDiffChange"> Ganze Zahl, 1-5
                </td>
            </tr>
            <tr>
                <td>
                    <p>Fische:</p><input type="text" id="dpFISHChange"> Ganze Zahl, 1-3
                </td>
                <td>
                    <p>Pflanzen:</p><input type="text" id="dpPLANTChange">Ganze Zahl, 1-3
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nachttauchen:</p><input type="text" id="dpNIGHTChange">Ganze Zahl, 1-3
                </td>
                <td>
                    <p>Tiefe:</p><input type="text" id="dpDEPTHChange">
                </td>
            </tr>
        </table>
        
        <input type="button" value="Speichern" onclick="changeDivingPlace()">

    </div>


    <h3>Löschen</h3>
    <div>
        <p>Tauchplatz</p><select id="deleteVerans">
            <?php

            $statement = 'SELECT * FROM DivingPlace ORDER BY name';
            $result = mysqli_query($connection, $statement);
            while ($ver = mysqli_fetch_assoc($result)) {
                echo '<option>' . utf8_encode($ver['name']) . '</option>';
            }


            ?>
        </select>
        <br>
        <input type="button" value="Löschen" onclick="deleteDivingPlace()">
    </div>

</div>
</body>
</html>
