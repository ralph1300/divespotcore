<?php
include("../initiateDB.php");
?>
<?php

$name = html_entity_decode(utf8_decode(urldecode($_REQUEST['name'])));
$long = $_REQUEST['long'];
$lati = $_REQUEST['lat'];
$lakeNAME = html_entity_decode(utf8_decode(urldecode($_REQUEST['lake'])));
$beschr = html_entity_decode(utf8_decode(urldecode($_REQUEST['beschr'])));
$beschrALT = html_entity_decode(utf8_decode(urldecode($_REQUEST['beschrALT'])));
$quality = $_REQUEST['qual'];
$diff = $_REQUEST['diff'];
$fish = $_REQUEST['fish'];
$plants = $_REQUEST['plants'];
$night = $_REQUEST['night'];
$depth = $_REQUEST['depth'];

$lakeID = 0;


//get lakeID from name

$getLake = "SELECT * FROM lake WHERE name='$lakeNAME'";
$resultInfo = mysqli_query($connection,$getLake);
if($resultInfo) {
    $Info = mysqli_fetch_assoc($resultInfo);
    $lakeID = $Info["lakeID"];
}

//save divingplace
$statement = "INSERT INTO DivingPlace(name,text,alternateText,latitude,longitude,lakeID)VALUES('" . ($name) . "','" . ($beschr) . "','" . ($beschrALT) . "','" . $lati . "','" . $long . "'," . $lakeID . ")";
mysqli_query($connection, $statement);

//get id of the place you just saved
$getPlace = "SELECT * FROM DivingPlace WHERE name='$name'";
$resultPlace = mysqli_query($connection, $getPlace);
if ($resultPlace) {
    $place = mysqli_fetch_assoc($resultPlace);
}
$placeID = $place['placeID'];

//save rating with placeID
$addRating = "INSERT INTO rating(depth,divingPlaceID,quality,difficulty,fish,plants,nightdive)VALUES('" . "$depth" . "'," . $placeID . "," . $quality . "," . $diff . "," . $fish . "," . $plants . "," . $night . ")";
mysqli_query($connection, $addRating);
echo json_encode(utf8_encode($name));


function utf8_urldecode($str) {
    $str = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($str));
    return html_entity_decode($str,null,'UTF-8');;
}

?>