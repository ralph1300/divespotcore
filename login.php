<html>

<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title>ADMINAREA</title>

    <link href="Styles/AdminStyle.css" rel="stylesheet" type="text/css">
    <link href="Styles/NavigationStyle.css" rel="stylesheet" type="text/css">
    <link href="Styles/BackgroundStyle.css" rel="stylesheet" type="text/css">
    <link href="Styles/LoginStyle.css" rel="stylesheet" type="text/css">
    <script src="JS/jScript.js" type="text/javascript"></script>
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
            filter: none;
        }

        .auto-style1 {
            margin: 0px;
            text-align: left;
            float: left;
        }
        label {
            margin-left:auto;
            margin-right:auto;
        }
        input {
            margin-left:auto;
            margin-right:auto;
        }
    </style>
    <![endif]-->


</head>
<body>
<div id="MainArea">
    <table>
        <tr>
            <td>
                <?php
                include("initiateDB.php");
                session_start();
                include_once('Login/sessionhelpers.inc.php');
                if (isset($_POST['login'])) {

                    $userid = check_user($_POST['username'], $_POST['userpass']);
                    if ($userid) {
                        login($userid);
                    } else {
                        echo '<p>Ihre Anmeldedaten waren nicht korrekt!</p>';
                    }
                }
                if (!logged_in()) {
                    echo <<<END
<form method="post" action="login.php">
<br>
<br>
<br>
<label>Benutzername:</label> <input name="username" type="text"><br />
<label>Passwort:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> <input name="userpass" type="password" id="userpass"><br />
<br>
<input align="right" name="login" type="submit" id="button" value="Einloggen">
</form>
END;
                } else {
                    $getUser = "SELECT * FROM adminuser";
                    $result = mysqli_query($connection, $getUser);
                    if ($result) {
                        $userData = mysqli_fetch_assoc($result);
                    }
                    header('Location: Admintest.php');
                }

                ?>
            </td>
            <td align="right">
                <br>
            </td>
        </tr>
    </table>
</div>


</body>
</html>