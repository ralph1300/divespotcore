var isProfil = true;
var isPics = false;
var isProfilInfo = true;
var isPicsInfo = false;
var connstring = "";
$(document).ready(initJquery);

function initJquery() {
    updateVisibility();
    updateVisibilityInfo();
}


//TODO: Fotos bei delete löschen, tauchschule tabelle als layout
//----------------------------------------------------DIVINGPLACE---------------------------------------------------------FERTIG
function saveDivingPlace() {
    var name = document.getElementById('dpNAME').value;
    var lat = document.getElementById('dpLAT').value;
    var long = document.getElementById('dpLONG').value;
    var lake = document.getElementById('dpLAKE').value;
    var beschr = document.getElementById('dpTEXT').value;
    var beschrALT = document.getElementById('dpTEXTALT').value;
    var quality = document.getElementById('dpQual').value;
    var diff = document.getElementById('dpDiff').value;
    var fish = document.getElementById('dpFISH').value;
    var plants = document.getElementById('dpPLANT').value;
    var night = document.getElementById('dpNIGHT').value;
    var depth = document.getElementById('dpDEPTH').value;
    if (name == "" || lat == "" || long == "" || beschr == "") {
        alert("Bitte alle notwendigen Daten angeben.");
    }
    else {

        var url = (connstring + "DivingPlace/saveDivingPlace.php?name=" + encodeURIComponent(name) + "&long=" +
        long + "&lat=" + lat + "&lake=" + encodeURIComponent(lake) + "&beschr=" + encodeURIComponent(beschr)
        + "&beschrALT=" + encodeURIComponent(beschrALT) + "&qual=" + quality + "&diff=" + diff
        + "&fish=" + fish + "&plants=" + plants + "&night=" + night + "&depth=" + depth);

        $.getJSON(url, function (json) {
            var x = json;
            alert("Tauchplatz -> " + x + " <- hinzugefügt");
            window.location.reload();
        });
    }
}
function updateCHDivingPlace() {
    //alert(document.getElementById('DIVINGPLACECHANGE').value);
    var name = document.getElementById('DIVINGPLACECHANGE').value;
    $.getJSON(connstring + "DivingPlace/getDivingPlace.php?name=" + encodeURIComponent(name), function (json) {
        var x = json;
        var arr = new Array();
        arr = x.toString().split('|');
        document.getElementById('dpDEPTHChange').value = arr.pop();
        document.getElementById('dpNIGHTChange').value = arr.pop();
        document.getElementById('dpPLANTChange').value = arr.pop();
        document.getElementById('dpFISHChange').value = arr.pop();
        document.getElementById('dpDiffChange').value = arr.pop();
        document.getElementById('dpQualChange').value = arr.pop();
        document.getElementById('dpLONGChange').value = arr.pop();
        document.getElementById('dpLATChange').value = arr.pop();
        document.getElementById('dpTEXTALTChange').value = arr.pop();
        document.getElementById('dpTEXTChange').value = arr.pop();
        document.getElementById('dpNAMEChange').value = arr.pop();
        document.getElementById('dpLAKEChange').value = arr.pop();
    });
}
function clearUI() {

    document.getElementById('dpDEPTHChange').value = "";
    document.getElementById('dpNIGHTChange').value = "";
    document.getElementById('dpPLANTChange').value = "";
    document.getElementById('dpFISHChange').value = "";
    document.getElementById('dpDiffChange').value = "";
    document.getElementById('dpQualChange').value = "";
    document.getElementById('dpLONGChange').value = "";
    document.getElementById('dpLATChange').value = "";
    document.getElementById('dpTEXTALTChange').value = "";
    document.getElementById('dpTEXTChange').value = "";
    document.getElementById('dpNAMEChange').value = "";

}

function loadSummaryForDivePlace() {
    var lakeName = document.getElementById("filterLakeForSummary").value;
    $.getJSON(connstring + "DivingPlace/getSummaryForLake.php?lakeName=" + encodeURIComponent(lakeName), function (json) {
        var x = json;
        var table = document.getElementById("summaryTable");
        table.innerHTML = "<tr><td>Name</td><td>Titelbild hochgeladen?</td><td>Hochgeladenen Bilder</td><td>Thumbnails</td></tr>"
        var arr = new Array();
        arr = x.toString().split('|');
        for (index = 0; index < arr.length; ++index) {
            createTableRow(arr[index]);
        }
    });
}

function createTableRow(value) {
    var thumbString = "/uploads/thumb/'"
    var arr = value.toString().split(';');
    var tr = document.createElement('tr');
    var isImgThere = "Nein";
    var imgCount = "0";
    var names = "Nicht verfügbar";
    if (arr[0] != "") {
        if (arr[1] != "") {
            isImgThere = "Ja";
        }
        names = arr[2];
        var images = "";
        var urls = names.toString().split('-');
        for (var index = 1; index < urls.length; index++) {
            images = images + createImage(thumbString + urls[index]);
        }
        imgCount = arr[3];
        tr.innerHTML = "<td>" + arr[0] + "</td><td>" + isImgThere + "</td><td>" + imgCount + "</td><td>" + images + "</td>";
        document.getElementById('summaryTable').appendChild(tr);
    }
}

function createImage(src) {
    src = src.replace("'","");
    return '<img src="'+src+'" title="'+""+'" alt="'+""+'" />';
}


function updateDivingPlacesWithLake() {
    var lakeName = document.getElementById('LAKECHANGEFORPLACE').value;
    clearUI();
    $.getJSON(connstring + "DivingPlace/getPlacesForLake.php?name=" + encodeURIComponent(lakeName), function (json) {
        var x = json;
        document.getElementById('DIVINGPLACECHANGE').innerHTML = '';
        var arr = new Array();
        arr = x.toString().split('|');
        for (index = 0; index < arr.length; ++index) {
            createOption(arr[index]);
        }
    });

}
function createOption(value) {
    el = document.createElement('option');
    el.value = value;
    el.innerHTML = value;
    el.id = value;

    document.getElementById('DIVINGPLACECHANGE').appendChild(el);
}
function changeDivingPlace() {
    var oldName = document.getElementById('DIVINGPLACECHANGE').value;
    var name = document.getElementById('dpNAMEChange').value;
    var lat = document.getElementById('dpLATChange').value;
    var long = document.getElementById('dpLONGChange').value;
    var lake = document.getElementById('dpLAKEChange').value;
    var beschr = document.getElementById('dpTEXTChange').value;
    var beschrALT = document.getElementById('dpTEXTALTChange').value;
    var quality = document.getElementById('dpQualChange').value;
    var diff = document.getElementById('dpDiffChange').value;
    var fish = document.getElementById('dpFISHChange').value;
    var plants = document.getElementById('dpPLANTChange').value;
    var night = document.getElementById('dpNIGHTChange').value;
    var depth = document.getElementById('dpDEPTHChange').value;
    if (name == "" || lat == "" || long == "" || beschr == "") {
        alert("Bitte alle notwendigen Daten angeben.");
    }
    else {
        $.getJSON(connstring + "DivingPlace/changeDivingPlace.php?name=" + encodeURIComponent(name) + "&oName=" + encodeURIComponent(oldName) + "&long=" +
            long + "&lat=" + lat + "&lake=" + lake + "&beschr=" + encodeURIComponent(beschr)
            + "&beschrALT=" + encodeURIComponent(beschrALT) + "&qual=" + quality + "&diff=" + diff
            + "&fish=" + fish + "&plants=" + plants + "&night=" + night + "&depth=" + depth, function (json) {
            var x = json;
            alert("Tauchplatz -> " + x + " <- geändert");
            window.location.reload();
        });
    }
}
function deleteDivingPlace() {
    var verans = document.getElementById('deleteVerans').value;
    $.getJSON(connstring + "DivingPlace/deleteDivingPlace.php?name=" + encodeURIComponent(verans), function (json) {
        var x = json;
        alert("Erfolgreich gelöscht");
        window.location.reload();
    });
}

//------------------------------------------------------!!DIVINGPLACE!!----------------------------------------------------FERTIG
//------------------------------------------------------LAKE---------------------------------------------------------------FERTIG

function deleteLake() {
    var info = document.getElementById('deleteInfo').value;
    $.getJSON(connstring + "Lake/deleteLake.php?name=" + encodeURIComponent(info), function (json) {
        var x = json;
        alert("See " + x + " wurde erfolgreich gelöscht!");
        window.location.reload();
    });
}
function changeLake() {
    var oldname = document.getElementById('INFOCHANGE').value;
    var newname = document.getElementById('Changetitel').value;
    var long = document.getElementById('ChangeLong').value;
    var lat = document.getElementById('ChangeLat').value;
    var isShopElement = document.getElementById('isShopeLakeChange');
    var isShop = 0;
    if (isShopElement.checked) {
        isShop = 1;
    }
    $.getJSON(connstring + "Lake/changeLake.php?nname=" + encodeURIComponent(newname) + "&long=" + long +
        "&lat=" + lat + "&oldname=" + encodeURIComponent(oldname) + "&isShop=" + isShop, function (json) {
        var x = json;
        alert("See -> " + x + " <- wurde erfolgreich geändert!");
        window.location.reload();
    });
}
function updateChangeLakeInfo() {
    var info = document.getElementById('INFOCHANGE').value;
    $.getJSON(connstring + "Lake/getLake.php?name=" + encodeURIComponent(info), function (json) {
        var x = json;
        var arr = new Array();
        arr = x.toString().split('|');

        var isShop = arr.pop();
        document.getElementById('ChangeLat').value = arr.pop();
        document.getElementById('ChangeLong').value = arr.pop();
        document.getElementById('Changetitel').value = arr.pop();
        
        if (isShop == 1) {
            document.getElementById('isShopeLakeChange').checked = true;
        } else {
            document.getElementById('isShopeLakeChange').checked = false;
        }
    });

}
function saveLake() {
    var name = document.getElementById('lakeName').value;
    var lat = document.getElementById('latitudeLake').value;
    var long = document.getElementById('longitudeLake').value;
    var isShopElement = document.getElementById('isShopeLake');
    var isShop = 0;
    if (isShopElement.checked) {
        isShop = 1;
    }
    $.getJSON(connstring + "Lake/saveLake.php?name=" + encodeURIComponent(name) + "&long=" + long +
        "&lat=" + lat + "&isShop=" + isShop, function (json) {
        var x = json;
        alert("See -> " + x + " <- wurde erfolgreich gespeichert!");
        window.location.reload();
    });
}
//------------------------------------------------------!!LAKE!!------------------------------------------------------------FERTIG
//------------------------------------------------------DIVINGSCHOOL--------------------------------------------------------
function saveDivingSchool() {
    var name = document.getElementById('dsNAME').value;
    var lat = document.getElementById('dsLAT').value;
    var long = document.getElementById('dsLONG').value;
    var lake = document.getElementById('dsLAKE').value;
    var beschr = document.getElementById('dsBESCH').value;
    var beschrALT = document.getElementById('dsBESCHALT').value;
    var url = document.getElementById('dsURL').value;

    if (name == "" || lat == "" || long == "" || beschr == "") {
        alert("Bitte alle notwendigen Daten angeben.");
    }
    else {
        $.getJSON(connstring + "DivingSchool/saveDivingSchool.php?name=" + encodeURIComponent(name) + "&long=" +
            long + "&lat=" + lat + "&lake=" + encodeURIComponent(lake) + "&beschr=" + encodeURIComponent(beschr)
            + "&beschrALT=" + encodeURIComponent(beschrALT) + "&url=" + encodeURIComponent(url), function (json) {
            var x = json;
            alert("Tauchschule -> " + x + " <- hinzugefügt");
            window.location.reload();
        });
    }
}
function updateDivingSchoolChange() {

    var name = document.getElementById('dsNAMECHANGESELECT').value;
    $.getJSON(connstring + "DivingSchool/getDivingSchool.php?name=" + encodeURIComponent(name), function (json) {
        //$place['name'] . "|" . $place['informationText'] . "|" . $place['alternateText'] . "|" . $place['webURL']. "|" . $place['latitude']. "|" . $place['longitude']. "|" . $lakeName);
        var x = json;
        var arr = new Array();
        arr = x.toString().split('|');
        document.getElementById('dsLAKECHANGE').value = arr.pop();
        document.getElementById('dsLONGCHANGE').value = arr.pop();
        document.getElementById('dsLATCHANGE').value = arr.pop();
        document.getElementById('dsURLCHANGE').value = arr.pop();
        document.getElementById('dsBESCHALTCHANGE').value = arr.pop();
        document.getElementById('dsBESCHCHANGE').value = arr.pop();
        document.getElementById('dsNAMECHANGE').value = arr.pop();
    });
}
function changeDivingSchool() {

    var oldName = document.getElementById('dsNAMECHANGESELECT').value;
    var name = document.getElementById('dsNAMECHANGE').value;
    var lat = document.getElementById('dsLATCHANGE').value;
    var long = document.getElementById('dsLONGCHANGE').value;
    var lake = document.getElementById('dsLAKECHANGE').value;
    var beschr = document.getElementById('dsBESCHCHANGE').value;
    var beschrALT = document.getElementById('dsBESCHALTCHANGE').value;
    var url = document.getElementById('dsURLCHANGE').value;

    url = url.replace("/", ";");
    if (name == "" || lat == "" || long == "" || beschr == "") {
        alert("Bitte alle notwendigen Daten angeben.");
    }
    else {
        $.getJSON(connstring + "DivingSchool/changeDivingSchool.php?name=" + encodeURIComponent(name) + "&oName=" + encodeURIComponent(oldName) + "&long=" +
            long + "&lat=" + lat + "&lake=" + encodeURIComponent(lake) + "&beschr=" + encodeURIComponent(beschr)
            + "&beschrALT=" + encodeURIComponent(beschrALT) + "&url=" + encodeURIComponent(url), function (json) {
            var x = json;
            alert("Tauchschule -> " + x + " <- geändert");
            window.location.reload();
        });
    }
}
function deleteDivingSchool() {
    var verans = document.getElementById('deleteDS').value;
    $.getJSON(connstring + "DivingSchool/deleteDivingSchool.php?name=" + encodeURIComponent(verans), function (json) {
        var x = json;
        alert("Erfolgreich gelöscht");
        window.location.reload();
    });
}
//------------------------------------------------------!!DIVINGSCHOOL!!--------------------------------------------------
//------------------------------------------------------IMAGES------------------------------------------------------------
function deleteImagesFromSchool() {
    var name = document.getElementById('SchoolPICS').value;
    $.getJSON(connstring + "Image/deleteImagesFromSchool.php?name=" + encodeURIComponent(name), function (json) {
        var x = json;
        alert("Erfolgreich gelöscht");
        window.location.reload();
    });
}
function deleteImagesFromPlace() {
    var name = document.getElementById('PlacePICS').value;
    $.getJSON(connstring + "Image/deleteImagesFromPlace.php?name=" + encodeURIComponent(name), function (json) {
        var x = json;
        alert("Erfolgreich gelöscht");
        window.location.reload();
    });
}

//------------------------------------------------------!!IMAGES!!--------------------------------------------------------
//------------------------------------------------------UPDATEVISIBILITY--------------------------------------------------
function changePics() {
    isPics = true;
    isProfil = false;
    updateVisibility();
}
function changeProfil() {
    isPics = false;
    isProfil = true;
    updateVisibility();

}
function changeProfilInfo() {
    isPicsInfo = false;
    isProfilInfo = true;
    updateVisibilityInfo();
}
function changePicsInfo() {
    isPicsInfo = true;
    isProfilInfo = false;
    updateVisibilityInfo();
}
function updateVisibility() {
    if (isPics == true) {
        jQuery("#PICS").show();
    }
    else {
        jQuery("#PICS").hide();
    }
    if (isProfil == true) {
        jQuery("#PROFILE").show();
    }
    else {
        jQuery("#PROFILE").hide();
    }
}
function updateVisibilityInfo() {
    if (isPicsInfo == true) {
        jQuery("#PICSINFO").show();
    }
    else {
        jQuery("#PICSINFO").hide();
    }
    if (isProfilInfo == true) {
        jQuery("#PROFILEINFO").show();
    }
    else {
        jQuery("#PROFILEINFO").hide();
    }
}
//------------------------------------------------------!!UPDATEVISIBILITY!!--------------------------------------------------
function goBack() {
    window.location.href = "../EditStartpage.html";
}
function goBack1() {
    window.location.href = "Archiv.html";
}