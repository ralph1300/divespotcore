var isProfil = true;
var isPics = false;
var isProfilInfo = true;
var isPicsInfo = false;
var connstring = "localhost/Divespot";
$(document).ready(initJquery);

function initJquery(){
	updateVisibility();
	updateVisibilityInfo();
}
//----------------------------------------------------VERANSTALTUNG------------------------------------------------
function SaveVeranstaltung(){
var name = document.getElementById('VeransName').value;
var preis  = document.getElementById('VeranPreis').value;
var Datum = document.getElementById('DatePicker1').value;
var Standpunkt = document.getElementById('Standpunkt').value;
var kBesch = document.getElementById('area').value;
var beschr = document.getElementById('Beschr').value;
var titel = document.getElementById('titel').value;
if(name== "" || preis== "" || Datum== "" || Standpunkt== "" || kBesch== "" || beschr== "" || titel== ""){
    alert("Bitte alle Daten angeben.");
}
else{
    $.getJSON(connstring+"Veranstaltung/SaveVerans.php?name=" + name + "&preis=" +
    preis + "&datum="+ Datum + "&standpunkt=" + Standpunkt + "&kBesch=" + kBesch
    + "&beschr=" + beschr + "&titel=" + titel , function(json){
    var x = json;
    alert("Veranstaltung -> " + x + " <- hinzugefügt");
    window.location.reload();
});
}
}
function UpdateChangeVerans(){
	var name = document.getElementById('Verans').value;
$.getJSON(connstring+"Veranstaltung/getDivingPlace.php?name=" + name, function(json){
	var x = json;
	var arr = new Array();
	arr = x.toString().split('|');
	document.getElementById('changetitel').value = arr.pop();
	document.getElementById('changeBeschr').value = arr.pop();
        var date = arr.pop();
        var arr1 = date.split('-');
        var newdate = arr1[2] + "/" + arr1[1] + "/" + arr1[0];
	document.getElementById('changePicker').value = newdate;
	document.getElementById('changeArea').value = arr.pop();
	document.getElementById('changePreis').value = arr.pop();
	document.getElementById('changeVerans').value = arr.pop();
});
}
function changeVerans(){ 
var verans = document.getElementById('Verans').value;
var name = document.getElementById('changeVerans').value;
var preis  = document.getElementById('changePreis').value;
var Datum = document.getElementById('changePicker').value;
var Standpunkt = document.getElementById('Standpunkt1').value;
var kBesch = document.getElementById('changeArea').value;
var beschr = document.getElementById('changeBeschr').value;
var titel = document.getElementById('changetitel').value;
if(name=="" || preis=="" || Datum=="" || Standpunkt=="" || kBesch=="" || beschr=="" || titel==""){
    alert("Bitte alle Daten angeben.");
}
else{
$.getJSON(connstring+"Veranstaltung/changeDivingPlace.php?name=" + name + "&preis=" +
		preis + "&datum="+ Datum + "&standpunkt=" + Standpunkt + "&kBesch=" + kBesch 
		+ "&beschr=" + beschr + "&titel=" + titel +"&verans="+verans, function(json){
		var x = json;
		alert("Veranstaltung -> " + x + " <- geändert");
		window.location.reload();
});
}
}
function deleteVerans(){ 
	var verans = document.getElementById('deleteVerans').value;
$.getJSON(connstring+"Veranstaltung/deleteDivingPlace.php?name=" + verans, function(json){
		var x = json;
		alert("Erfolgreich gelöscht");
});
}

//------------------------------------------------------!!VERANSTALTUNG!!--------------------------------------------------
//------------------------------------------------------INFO---------------------------------------------------------------

function deleteInfo(){
    var info = document.getElementById('deleteInfo').value;
    $.getJSON(connstring+"Veranstaltung/INFO/deleteLake.php?name=" + info, function(json){
		var x = json;
		alert("Info " + x + " wurde erfolgreich gelöscht!");
});
}
function changeInfo(){
var oldname = document.getElementById('INFOCHANGE').value;
var newname = document.getElementById('Changetitel').value;
var date    = document.getElementById('ChangePicker').value;
var punkt   = document.getElementById('changeStandpunkt').value;
var title   = document.getElementById('changettitle').value;
var text    = document.getElementById('changeArea1').value;
$.getJSON(connstring+"Veranstaltung/INFO/changeLake.php?nname=" + newname + "&date=" + date +
            "&punkt=" + punkt + "&text=" + text + "&title=" + title + "&oldname=" + oldname, function(json){
		var x = json;
		alert("Info -> " +x+ " <- wurde erfolgreich geändert!");
});
}
function updateChangeInfo(){
var info = document.getElementById('INFOCHANGE').value;
$.getJSON(connstring+"Veranstaltung/INFO/getLake.php?name=" + info, function(json){
	var x = json;
	var arr = new Array();
	arr = x.toString().split('|');
        document.getElementById('changettitle').value = arr.pop();
        document.getElementById('changeArea1').value = arr.pop();
        var date = arr.pop();
        var arr1 = date.split('-');
        var newdate = arr1[2] + "/" + arr1[1] + "/" + arr1[0];
	document.getElementById('ChangePicker').value = newdate;
        document.getElementById('Changetitel').value = arr.pop();
    });
    
}
function saveInfo(){
var name = document.getElementById('Infotitle').value;
var date = document.getElementById('InfoPicker').value;
var punkt = document.getElementById('StandpunktSel').value;
var text = document.getElementById('areaText').value;
var title = document.getElementById('ttitle').value;
$.getJSON(connstring+"Veranstaltung/INFO/saveLake.php?name=" + name + "&date=" + date +
            "&punkt=" + punkt + "&text=" + text + "&title=" + title, function(json){
		var x = json;
		alert("Info -> " + x + " <- wurde erfolgreich gespeichert!");
                window.location.reload();
});
}
function saveLake(){
	var name = document.getElementById('Infotitle').value;
	var date = document.getElementById('InfoPicker').value;
	var punkt = document.getElementById('StandpunktSel').value;
	var text = document.getElementById('areaText').value;
	var title = document.getElementById('ttitle').value;
	$.getJSON(connstring+"Veranstaltung/INFO/saveLake.php?name=" + name + "&date=" + date +
		"&punkt=" + punkt + "&text=" + text + "&title=" + title, function(json){
		var x = json;
		alert("Info -> " + x + " <- wurde erfolgreich gespeichert!");
		window.location.reload();
	});
}
//------------------------------------------------------!!INFO!!------------------------------------------------------------
//------------------------------------------------------ARCHIV--------------------------------------------------------------
function deleteArchiv(){
var archiv = document.getElementById('archivID').value;
	$.getJSON(connstring+"Fotoarchiv/deleteArchiv.php?name=" + archiv, function(json){
		var x = json;
		alert("Archiv erfolgreich gelöscht");
	});
}
function UpdateChangeArchiv(){
var archiv = document.getElementById('Archiv').value;
$.getJSON(connstring+"Fotoarchiv/getArchiv.php?name=" + archiv, function(json){
	var x = json;
	var arr = new Array();
	arr = x.toString().split('|');
	document.getElementById('changetexttitel').value = arr.pop();
	document.getElementById('changearchivbesch').value = arr.pop();	
	document.getElementById('changeArchivTitel').value = arr.pop();
});
}
function changeArchiv(){
var archiv = document.getElementById('Archiv').value;
var name = document.getElementById('changeArchivTitel').value;
var punkt = document.getElementById('changepunkt').value;
var besch = document.getElementById('changearchivbesch').value;
var ttitle = document.getElementById('changeArchivTitel').value;
$.getJSON(connstring+"Fotoarchiv/changeArchiv.php?name=" + name  + "&besch=" +
		besch + "&punkt="+ punkt + "&ttitle=" + ttitle +"&archiv=" + archiv , function(json){
		var x = json;
		alert("Archiv erfolgreich geändert");
});
}
function FinishGoto(){
var titel = document.getElementById('ArchivTitel').value;
var besch = document.getElementById('archivbesch').value;
var punkt = document.getElementById("punkt").value;
var ttitel = document.getElementById('texttitel').value;
var save = "yes";
$.getJSON(connstring+"Fotoarchiv/saveArchiv.php?name=" + titel  + "&besch=" +
	besch + "&punkt="+ punkt + "&save=" + save +"&ttitle=" + ttitel , function(json){
	var x = json;
	alert("Archiv erfolgreich hinzugefügt");
});
}
//------------------------------------------------------!!ARCHIV!!--------------------------------------------------
//------------------------------------------------------UPDATEVISIBILITY--------------------------------------------------
function changePics(){
	isPics = true;
	isProfil = false;
	updateVisibility();
}
function changeProfil(){
	isPics = false;
	isProfil = true;
	updateVisibility();

}
function changeProfilInfo(){
	isPicsInfo = false;
	isProfilInfo = true;
	updateVisibilityInfo();
}
function changePicsInfo(){
	isPicsInfo = true;
	isProfilInfo = false;
	updateVisibilityInfo();
}
function updateVisibility(){
	if(isPics == true){
		jQuery("#PICS").show();
	}
	else{
		jQuery("#PICS").hide();
	}
	if(isProfil == true){
		jQuery("#PROFILE").show();
	}
	else{
		jQuery("#PROFILE").hide();
	}
}
function updateVisibilityInfo(){
	if(isPicsInfo == true){
		jQuery("#PICSINFO").show();
	}
	else{
		jQuery("#PICSINFO").hide();
	}
	if(isProfilInfo == true){
		jQuery("#PROFILEINFO").show();
	}
	else{
		jQuery("#PROFILEINFO").hide();
	}
}
//------------------------------------------------------!!UPDATEVISIBILITY!!--------------------------------------------------
function goBack(){
    window.location.href = "../Veranstaltung.html";
}
function goBack1(){
    window.location.href = "Archiv.html";
}