function createChild(){
    var vname = document.getElementById('vname').value;
    var lname = document.getElementById('nname').value;
    var dat = document.getElementById('kindDatepicker').value;
    var gr = document.getElementById('group').value;
    $.getJSON("//localhost/Adminbereich/Admin/Kinder/InsertPHP.php?vname=" +
        vname + "&lname=" + lname + "&dat=" + dat + "&gr=" + gr,
    function(json){
        var x = json;
        alert('Das Kind wurde erfolgreich hinzugefügt.');
        window.location.reload();
    });
    
}
function deleteChild(){

    var name = document.getElementById('delName');
    name = name.value;
    var arr = new Array();
    arr = name.toString().split(' ');
    var vname = arr.pop();
    var nname = arr.pop();
    $.getJSON("//localhost/Adminbereich/Admin/Admin/Kinder/DeletePHP.php?vname=" + vname + "&nname=" + nname, function(json){
        var x = json;
        alert('Löschvorgang erfolgreich');
        window.location.reload();
    });
}
function LoadData(){
    var name = document.getElementById('changeName');
    name = name.value;
    var arr = new Array();
    arr = name.toString().split(' ');
    var vname = arr.pop();
    var nname = arr.pop();
    document.getElementById('KVname').value= vname;
    document.getElementById('KNname').value= nname;
    var res = null;
    $.getJSON("//localhost/Adminbereich/Admin/Kinder/ChangePHP.php?vname =" + vname + "&nname=" + nname, function(json){
        res = json;
        alert(res);
        var array = new Array();
        array = res.toString().split(' ');
        var grpID = array.pop();
        var gebdat = array.pop();
        document.getElementById('Kgrp').value= grpID;
        document.getElementById('kindDatepicker2').value= gebdat;
    });
}
function ChangeKid(){
    var name = document.getElementById('changeName');
    name = name.value;
    var arr = new Array();
    arr = name.toString().split(' ');
    var oldvname = arr.pop();
    var oldnname = arr.pop();
    var vname = document.getElementById('KVname').value;
    var nname = document.getElementById('KNname').value;
    var gebdat = document.getElementById('kindDatepicker2').value;
    var grp = document.getElementById('Kgrp').value;
     $.getJSON("//localhost/Adminbereich/Admin/Kinder/ChangeKidPHP.php?oldvname=" + oldvname + "&oldnname=" + oldnname +
        "&vname=" + vname + "&nname" + nname + "&gebdat=" + gebdat + "&grp=" + grp, function(json){
        res = json;
        alert(res);
        window.location.reload();
    });
}
function CreateGroup(){
   var grpn = document.getElementById('GrpName').value;
   var erz  = document.getElementById('erz').value;
   var hel = document.getElementById('hel').value;
   var bild = document.getElementById('bildname').value;
   $.getJSON("//localhost/Adminbereich/Admin/Gruppen/CreateGroup.php?grpn=" + grpn + "&erz=" + erz +
        "&hel=" + hel + "&bild=" + bild, function(json){
        alert("Gruppe " + json + " wurde erfolgreich erzeugt");
        window.location.href="//localhost/Adminbereich/Admin.html";
    });
}
function delGruppe(){
    var grpn = document.getElementById('grpDel').value;
    $.getJSON("//localhost/Adminbereich/Admin/Gruppen/DeleteGroup.php?grpn=" + grpn, function(json){
        alert("Gruppe " + json + " wurde erfolgreich gelöscht");
        window.location.reload();
    });
}
function changeGrp(){
   var oldgrp = document.getElementById('grpChange').value;
   var grpn = document.getElementById('grp').value;
   var erz  = document.getElementById('erzi').value;
   var hel = document.getElementById('heli').value;
    $.getJSON("//localhost/Adminbereich/Admin/Gruppen/ChangeGroup.php?grpn=" + grpn + "&erz=" + erz +
        "&hel=" + hel + "&oldgrp=" + oldgrp, function(json){
        alert("Gruppe " + json + " wurde erfolgreich geaendert");
        window.location.reload();
    });
}
function addPerson(){
var vname = document.getElementById('vname').value;
var nname = document.getElementById('nname').value;
var beruf = document.getElementById('berufSe').value;
var gebdate = document.getElementById('personaldatepicker').value;
var bild = document.getElementById('bildname').value;
var beschr = document.getElementById('area').value;
 $.getJSON("//localhost/Adminbereich/Admin/Personal/AddPerson.php?vname=" + vname + "&nname=" + nname +
        "&beruf=" + beruf + "&gebdate=" + gebdate + "&bild1=" + bild + "&beschr="+beschr, function(json){
        alert("Personal " + json + " wurde erfolgreich hinzugefuegt");
        window.location.href="//localhost/Adminbereich/Admin.html";
    });
    
}
function delPerson(){
    var name = document.getElementById('delMitarb').value;
    var arr = new Array();
    arr = name.toString().split(' ');
    var nname = arr.pop();
    var vname = arr.pop();
    $.getJSON("//localhost/Adminbereich/Admin/Personal/DelPerson.php?vname=" + vname + "&nname=" + nname
        , function(json){
        alert("Loeschen des Mitarbeiters/in " + json + " war erfolgreich");
        window.location.reload();
    });
}
function FillPerson(){
    var nameid = document.getElementById('oldMitarb').value;
    var arr = new Array();
    arr = nameid.toString().split(' ');
    var id = arr.pop();
    var nname = arr.pop();
    var vname = arr.pop();
    document.getElementById('vnamenew').value=vname;
    document.getElementById('nnamenew').value=nname;
}
function changePerson(){
    var nameid = document.getElementById('oldMitarb').value;
    var arr = new Array();
    arr = nameid.toString().split(' ');
    var id = arr.pop();
    var vname = document.getElementById('vnamenew').value;
    var nname = document.getElementById('nnamenew').value;
    var newgebdate = document.getElementById('personalDatepicker').value;
    var newberuf = document.getElementById('newberuf').value;
    $.getJSON("//localhost/Adminbereich/Admin/Personal/ChangePerson.php?id=" + id + "&vname=" + vname + "$nname=" + nname + "&newgebdate=" +
        newgebdate + "&newberuf=" + newberuf
        , function(json){
        alert("Loeschen des Mitarbeiters/in " + json + " war erfolgreich");
        window.location.reload();
    });
}
function newTermin(){
    var name = document.getElementById('terminname').value;
    var date = document.getElementById('terminDatepicker').value;
    var besch = document.getElementById('terminbesch').value;
    $.getJSON("//localhost/Adminbereich/Admin/Termin/AddTermin.php?name=" + name + "&date=" + date + "& besch=" + besch
        , function(json){
        alert("Hinzufuegen des Termins " + json + " war erfolgreich");
        window.location.reload();
    });
    
}
function deleteTermin(){
    var name = document.getElementById('delTermin').value;
     $.getJSON("//localhost/Adminbereich/Admin/Termin/DeleteTermin.php?name=" + name
        , function(json){
        alert("Loeschen des Termins " + json + " war erfolgreich");
        window.location.reload();
    });
}
function ChangeTermin(){
   var name = document.getElementById('TerminAend').value;
   var newname = document.getElementById('terminbezneu').value;
   var besch = document.getElementById('terminbeschneu').value;
   var date = document.getElementById('termindatepickerneu').value;
    $.getJSON("//localhost/Adminbereich/Admin/Termin/ChangeTermin.php?name=" + name + "&date=" + date + "& besch=" + besch
        + "&newname=" + newname, function(json){
        alert("Aendern des Termins " + json + " war erfolgreich");
        window.location.reload();
    });
}
function CheckLogin(){
    var uname = document.getElementById('username').value;
    var pw = document.getElementById('userpw').value;
    $.getJSON("//localhost/Adminbereich/Admin/checkLogin.php?username=" + uname + "&pw=" + pw, function(json){
        if(json == 1){
        window.location.href ="//localhost/Adminbereich/Admin.html" ;
        }
        else{
            alert("Fehler - Falscher Username/Passwort");
        }
    });
}
function foward(){
    window.location.href="//localhost/Adminbereich/Admin/Gruppen/BildLoad.php";
}
function foward1(){
    window.location.href="//localhost/Adminbereich/Admin/Personal/BildLoad.php";
}
