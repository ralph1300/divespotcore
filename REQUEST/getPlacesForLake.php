<?php
/**
 * Created by PhpStorm.
 * User: ralphschnalzenberger
 * Date: 07/05/16
 * Time: 12:40
 */

include("../initiateDB.php");

mysqli_set_charset($connection,'utf8');

$id = $_REQUEST['id'];

$getPlaces = "SELECT DivingPlace.placeID,name,text,alternateText,latitude,longitude,lakeID,placeImage.url 
              FROM DivingPlace
              LEFT JOIN placeImage ON (DivingPlace.placeID=placeImage.placeID AND placeImage.isMainImg=1)
              WHERE lakeID='$id'";
$resultPlaces = mysqli_query($connection,$getPlaces);


$jsonArrayPlaces = array();
if($resultPlaces) {
    while ($place = mysqli_fetch_assoc($resultPlaces)) {
        array_push($jsonArrayPlaces, $place);

    }
}

$getSchools = "SELECT divingSchool.*,schoolImage.url
               FROM divingSchool 
               LEFT JOIN schoolImage ON (divingSchool.placeID=schoolImage.schoolID AND schoolImage.isMainImg=1)
               WHERE lakeID='$id'";
$resultSchools = mysqli_query($connection,$getSchools);
$jsonArraySchools = array();
if($resultSchools) {
    while ($school = mysqli_fetch_assoc($resultSchools)) {
        array_push($jsonArraySchools, $school);

    }
}

$str = "{ place:";
$jsonPlaces = json_encode($jsonArrayPlaces);
$jsonSchools = json_encode($jsonArraySchools);

$jsonString = $str . $jsonPlaces . ", schools:" . $jsonSchools . "}";

echo $jsonString;
