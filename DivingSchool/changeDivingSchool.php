<?php
include("../initiateDB.php");
?>
<?php

$oldname = html_entity_decode(utf8_decode(urldecode($_REQUEST['oName'])));
$name = html_entity_decode(utf8_decode(urldecode($_REQUEST['name'])));
$lakeName = html_entity_decode(utf8_decode(urldecode($_REQUEST['lake'])));
$lat = $_REQUEST['lat'];
$long = $_REQUEST['long'];
$beschr = html_entity_decode(utf8_decode(urldecode($_REQUEST['beschr'])));
$beschALT = html_entity_decode(utf8_decode(urldecode($_REQUEST['beschrALT'])));
$webURL = html_entity_decode(utf8_decode(urldecode($_REQUEST['url'])));

//get old school

$getOldSchool = "SELECT * FROM divingSchool WHERE name= '$oldname'";
$oldSchoolResult = mysqli_query($connection, $getOldSchool);
if ($oldSchoolResult) {
    $oldSchool = mysqli_fetch_assoc($oldSchoolResult);
}
$schoolID = $oldSchool['placeID'];

//get lakeID from name

$getLake = "SELECT * FROM lake WHERE name='$lakeName'";
$resultInfo = mysqli_query($connection,$getLake);
if($resultInfo) {
    $Info = mysqli_fetch_assoc($resultInfo);
    $lakeID = $Info["lakeID"];
}

//update school

$replacedURL = str_replace(";","/",$webURL);

$updateSchool = "UPDATE divingSchool SET name = '$name',text = '$beschr', alternateText='$beschALT',latitude='$lat',longitude = '$long',webURL = '$replacedURL', lakeID = $lakeID WHERE placeID='$schoolID'";
mysqli_query($connection,$updateSchool);
echo json_encode(utf8_encode($name));


?>