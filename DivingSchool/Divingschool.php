<!DOCTYPE html >
<?php
include("../initiateDB.php");
?>
<html>
<head>
    <link href="Styles/BackgroundStyle.css" rel="stylesheet" type="text/css">
    <link href="Styles/NavigationStyle.css" rel="stylesheet" type="text/css">
    <!--<link href= "Styles/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css">-->
    <script src="JQuery/jquery-1.7.1.js"></script>
    <script src="JQuery/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="JQuery/jquery.ui.widget.js"></script>
    <script src="JQuery/jquery.ui.tabs.js"></script>
    <script src="JQuery/jquery.ui.button.js"></script>
    <script src="JQuery/jquery.ui.accordion.js"></script>
    <script src="JQuery/jquery.ui.core.js"></script>
    <script src="JQuery/jquery.ui.datepicker.js"></script>
    <script src="JS/VScript.js"></script>
    <script>
        $(function () {
            $("#SchoolAccord").accordion();
        });

    </script>

    <title>Tauchbasis</title>
</head>
<body>
<h2>Tauchbasis</h2>
<div id="SchoolAccord">
    <h3>Erstellen</h3>
    <div>
        <table>
            <tr>
                <td>
                    <p>Name:</p><input type="text" id="dsNAME">
                </td>
                <td>
                    <p>URL:</p><input type="text" id="dsURL">
                </td>
            </tr>
            <tr>
                <td>
                    <p>Breitengrad:</p><input type="text" id="dsLONG"> Bsp.: 48.1234
                </td>
                <td>
                    <p>Längengrad:</p><input type="text" id="dsLAT"> Bsp.: 12.1234
                </td>
            </tr>
            <tr>
                <td>
                    <p>See/Gebiet:</p><select id="dsLAKE">
                        <?php
                        $statement = 'SELECT * FROM lake ORDER BY name';
                        $result = mysqli_query($connection, $statement);
                        while ($St = mysqli_fetch_assoc($result)) {
                            echo '<option>' . utf8_encode($St['name']) . '</option>';
                        }
                        ?>
                    </select>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <p>Beschreibung:</p><textarea id="dsBESCH" rows="5" cols="30"></textarea>
                </td>
                <td>
                    <p>Englische Beschreibung:</p><textarea id="dsBESCHALT" rows="5" cols="30"></textarea>
                </td>
            </tr>

        </table>
        <input type="button" value="Speichern" onclick="saveDivingSchool()"/>
    </div>


    <h3>Ändern</h3>
    <div>
        <p>Tauchbasis</p><select id="dsNAMECHANGESELECT" onchange="updateDivingSchoolChange()">
            <option></option>
            <?php

            $statement = 'SELECT * FROM divingSchool ORDER BY name';
            $result = mysqli_query($connection, $statement);
            while ($ver = mysqli_fetch_assoc($result)) {
                echo '<option>' . utf8_encode($ver['name']) . '</option>';
            }
            ?>
        </select>
        <table>
            <tr>
                <td>
                    <p>Name:</p><input type="text" id="dsNAMECHANGE">
                </td>
                <td>
                    <p>URL:</p><input type="text" id="dsURLCHANGE">
                </td>
            </tr>
            <tr>
                <td>
                    <p>Breitengrad:</p><input type="text" id="dsLONGCHANGE">
                </td>
                <td>
                    <p>Längengrad:</p><input type="text" id="dsLATCHANGE">
                </td>
            </tr>
            <tr>
                <td>
                    <p>See/Gebiet:</p><select id="dsLAKECHANGE">
                        <?php
                        $statement = 'SELECT * FROM lake ORDER BY name';
                        $result = mysqli_query($connection, $statement);
                        while ($St = mysqli_fetch_assoc($result)) {
                            echo '<option>' . utf8_encode($St['name']) . '</option>';
                        }
                        ?>
                    </select>
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <p>Beschreibung:</p><textarea id="dsBESCHCHANGE" rows="5" cols="30"></textarea>
                </td>
                <td>
                    <p>Englische Beschreibung:</p><textarea id="dsBESCHALTCHANGE" rows="5" cols="30"></textarea>
                </td>
            </tr>

        </table>



        <input type="button" value="Speichern" onclick="changeDivingSchool()"/>

    </div>


    <h3>Löschen</h3>
    <div>
        <p>Tauchbasis</p><select id="deleteDS">
            <?php

            $statement = 'SELECT * FROM divingSchool ORDER BY name';
            $result = mysqli_query($connection, $statement);
            while ($ver = mysqli_fetch_assoc($result)) {
                echo '<option>' . utf8_encode($ver['name']) . '</option>';
            }


            ?>
        </select>
        <br>
        <input type="button" value="Löschen" onclick="deleteDivingSchool()">
    </div>

</div>
</body>

</html>
