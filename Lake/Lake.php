<!DOCTYPE html>
<html>
<?php
include("../initiateDB.php");
?>
<head>
    <title>See</title>
    <link href="Styles/BackgroundStyle.css" rel="stylesheet" type="text/css">
    <link href="Styles/NavigationStyle.css" rel="stylesheet" type="text/css">
    <!--<link href= "Styles/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css">-->
    <script src="JQuery/jquery-1.7.1.js"></script>
    <script src="JQuery/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="JQuery/jquery.ui.widget.js"></script>
    <script src="JQuery/jquery.ui.tabs.js"></script>
    <script src="JQuery/jquery.ui.button.js"></script>
    <script src="JQuery/jquery.ui.accordion.js"></script>
    <script src="JQuery/jquery.ui.core.js"></script>
    <script src="JQuery/jquery.ui.datepicker.js"></script>
    <script src="JS/VScript.js"></script>
    <script>
        $(function () {
            $("#Infoaccord").accordion();
        });
        $(function () {
            $("#InfoPicker").datepicker();
        });
        $(function () {
            $("#ChangePicker").datepicker();
        });

    </script>
</head>

<body>
<h2>See/Tauchgebiet</h2>
<div id="Infoaccord">
    <h3>Erstellen</h3>
    <div>
        <p>Name:</p><input type="text" id="lakeName">
        <p>Breitengrad:</p><input type="text" id="longitudeLake"> Bsp.: 48.1234
        <p>Längengrad:</p><input type="text" id="latitudeLake"> Bsp.: 12.1234
        <form>
        <p>Ist dies ein Gebiet für Tauchshops?</p>
        <fieldset>
            <input type="checkbox" id="isShopeLake" name="isShop" value="ist Shop">
            <label for="isShopeLake"> ist Shop Gebiet</label> 
        </fieldset>
</form>
        <input type="button" value="Speichern" onclick="saveLake()">
    </div>

    <h3>Ändern</h3>
    <div>
        <p>See/Tauchgebiet:</p><select id="INFOCHANGE" onchange="updateChangeLakeInfo()">
            <option></option>
            <?php

            $statement = 'SELECT * FROM lake ORDER BY name';
            $result = mysqli_query($connection, $statement);
            while ($ver = mysqli_fetch_assoc($result)) {
                echo '<option>' . utf8_encode($ver['name']) . '</option>';
            }


            ?>
        </select>
        <p>Name:</p><input type="text" id="Changetitel">
        <p>Breitengrad:</p><input type="text" id="ChangeLong">
        <p>Längengrad:</p><input type="text" id="ChangeLat">
        <fieldset>
            <input type="checkbox" id="isShopeLakeChange" name="isShop" value="ist Shop">
            <label for="isShopeLakeChange"> ist Shop Gebiet</label> 
        </fieldset>
        <input type="button" value="Ändern" onclick="changeLake()">
    </div>
    <h3>Löschen</h3>
    <div>
        <p>See/Tauchgebiet</p>
        <select id="deleteInfo">
            <?php

            $statement = 'SELECT * FROM lake ORDER BY name';
            $result = mysqli_query($connection, $statement);
            while ($ver = mysqli_fetch_assoc($result)) {
                echo '<option>' . utf8_encode($ver['name']) . '</option>';
            }


            ?>
        </select>
        <input type="button" value="Löschen" onclick="deleteLake()">

    </div>

</div>
</body>

</html>
